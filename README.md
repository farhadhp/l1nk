# l1nk
A  link shorter for terminal

## Installation
1) Open terminal
2) insert `sudo ./install` and enter

## How to use
1) Open terminal
2) insert `l1nk http://YOUR-LONG-LINK.com` and enter

## License
This is free software -> GPL : more information -> https://fsf.org






